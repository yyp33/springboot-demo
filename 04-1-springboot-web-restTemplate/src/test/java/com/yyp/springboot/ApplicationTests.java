package com.yyp.springboot;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

@SpringBootTest
class ApplicationTests {

	@Test
	void restTemplate() {
		TestRestTemplate testRestTemplate = new TestRestTemplate();
		ResponseEntity<Result> exchange = testRestTemplate.exchange("http://localhost:8080/user/{id}", HttpMethod.DELETE,null, Result.class, 5);
		System.out.println(exchange);
	}

}
