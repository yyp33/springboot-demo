package com.yyp.springboot.entity;

public class User {
    private Integer id;
    private String userName;
    private Integer age;
    private String desc;



    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", userName='" + userName + '\'' + ", age=" + age + ", desc='" + desc + '\'' + '}';
    }

    public User() {
    }

    public User(int id, String userName, int age, String desc) {
        this.id = id;
        this.userName = userName;
        this.age = age;
        this.desc = desc;
    }
}
