package com.yyp.springboot.controller;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.yyp.springboot.Result;
import com.yyp.springboot.entity.User;

/**
 * 通过RestTemplate调用http接口
 * 在测试中使用TestRestTemplate
 * 适用于微服务架构下  服务之间的远程调用     ps: 以后使用微服务架构， spring cloud feign
 *
 */
@RestController
@RequestMapping("/restTemplate")
public class RestTemplateController {

    private RestTemplate restTemplate;

    /**
     * spring创建对象时如果没有无参构造方法，会调用有参数构造方法，但是参数必须是存在于spring容器中
     * @param restTemplateBuilder
     */
    public RestTemplateController(RestTemplateBuilder restTemplateBuilder){
        this.restTemplate = restTemplateBuilder.build();
    }

    /**
     * 调用远程的查询方法
     * @return
     */
    @GetMapping("/queryUser/{id}")
    public Result queryUser(@PathVariable Integer id){
        Result forObject = restTemplate.getForObject("http://localhost:8080/user/{id}", Result.class,  id);
        return forObject;
    }

    /**
     * 调用远程的新增方法
     * String url
     * Object request 请求参数
     * Class<T> responseType 返回值类型
     * Object... uriVariables url中占位符值
     * @return
     */
    @PostMapping("/addUser")
    public Result addUser(@RequestBody User user){
//        Result forObject = restTemplate.postForObject("http://localhost:8080/user/add", user,Result.class);
        ResponseEntity<Result> resultResponseEntity = restTemplate.postForEntity("http://localhost:8080/user/add", user, Result.class);
        System.out.println(resultResponseEntity);
        return resultResponseEntity.getBody();
    }

    /**
     * 调用远程的修改方法
     * @return
     */
    @PostMapping("/editUser/{id}")
    public Result editUser(@RequestBody User user){
        HttpEntity<User> httpEntity = new HttpEntity<>(user);
        ResponseEntity<Result> exchange = restTemplate.exchange("http://localhost:8080/user/{id}", HttpMethod.PUT, httpEntity, Result.class, 1);
        return exchange.getBody();
    }
    /**
     * 调用远程的修改方法
     * @return
     */
    @PostMapping("/deleteUser/{id}")
    public Result editUser(@PathVariable Integer id){
        ResponseEntity<Result> exchange = restTemplate.exchange("http://localhost:8080/user/{id}", HttpMethod.DELETE,null, Result.class, id);
        return exchange.getBody();
    }

}
