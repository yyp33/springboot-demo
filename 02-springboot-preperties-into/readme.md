##SpringBoot 配置文件属性注入
- 将YML配置文件中的属性，映射到对象中
    - YML配置文件注意事项
        - 格式：K: V(冒号与V之间有一个空格)
        - V如果通过""双引号包含特殊字符会被转义成要表达的方式
        - V如果通过''单引号包含特殊字符不会被转义，会原样展示
    - 对象、Map的映射
    ```
    //K: V 多行方式
    User:
      username: zhangsan
      age: 18
    //K: V 单行方式
    User: {username: zhangsan,age: 18}
    ```
    - 数组（list, set）映射
    ```
    //多行形式
      users:
          - zhangsan
          - lisi
    // 单行形式
      users: [zhangsan,lisi]
    ```
    - 配置文件如何与对象绑定
        - 对象上增加注解
        ```
         @Component//将对象交给spring管理
         @ConfigurationProperties(prefix = "person")//使用配置文件与对象属性绑定
         ，匹配配置文件中前缀：person
        ```
    - 松散绑定，如user对象有属性username
        - user_name
        - userName
        - user-name
        - USERNAME<br>
    以上四种方式的的定义都会不配到username这个属性上
    - @Value与@ConfigurationProperties
        - @Value 是获取单个属性，可以使用slfj表达式
        - @ConfigurationProperties 是自动批量匹配
        - 如果说，我们只是在某个业务逻辑中需要获取一下配置文件中的某项值，使用@Value；
          如果说，我们专门编写了一个javaBean来和配置文件进行映射，我们就直接使用@ConfigurationProperties；
        - @Value使用
        ```text
        @Value("${person.username}")
        private String name;
        @Value("#{11*10}")
        private int age;
        @Value("true")
        private boolean man;  
        ```
    - @PropertiesSource
        - 标记用户类上用于配置使用自定义properties配置文件
    - 占位符
        - 随机数
        ```text
        ${random.value}
        ${random.int}
        ${random.long}
        ```
        - 使用之前定义的配置
        ```text
         person.username=zhangsan
         person.desc=${person.username}的描述
        ```