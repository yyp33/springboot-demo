package com.yyp.springboot;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.yyp.springboot.entity.Person;

@SpringBootTest
class ApplicationTests {

	@Autowired
	Person person;


	/**
	 * 验证配置文件属性与对象属性绑定
	 */
	@Test
	void contextLoads() {
		System.out.println(person);
	}

}
