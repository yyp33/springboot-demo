package com.yyp.springboot.entity;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @ConfigurationProperties
 * 用于标记将配置文件中定义的属性注入到对象中
 * prefix 用于标记将那个前缀下的属性注入
 *
 * 如果想要配置生效需要将对象放入spring容器进行管理所以需要使用@Component注解
 *
 * 具体的使用可以看yml配置文件
 */
@PropertySource("classpath:person.properties")
@Component
@ConfigurationProperties(prefix = "person")
public class Person {

    private String username;
    private Integer age;
    private List<String> friends;
    private Map<String, String> map;
    private Date birthDay;
    private String[] hobbies;
    private Wife wife;
    private String desc;
    /**
     * 使用@Value或者配置文件中默认配置的值
     * 是使用SLFJ表达式
     */
    @Value("${person.user-name}")
    private String name;
    @Value("#{11*10}")
    private int money;
    @Value("true")
    private boolean man;
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public List<String> getFriends() {
        return friends;
    }

    public void setFriends(List<String> friends) {
        this.friends = friends;
    }

    public Map<String, String> getMap() {
        return map;
    }

    public void setMap(Map<String, String> map) {
        this.map = map;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    public String[] getHobbies() {
        return hobbies;
    }

    public void setHobbies(String[] hobbies) {
        this.hobbies = hobbies;
    }

    public Wife getWife() {
        return wife;
    }

    public void setWife(Wife wife) {
        this.wife = wife;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public boolean isMan() {
        return man;
    }

    public void setMan(boolean man) {
        this.man = man;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "Person{" + "username='" + username + '\'' + ", age=" + age + ", friends=" + friends + ", map=" + map + ", birthDay=" + birthDay + ", hobbies=" + Arrays.toString(hobbies) + ", wife=" + wife + ", desc='" + desc + '\'' + ", name='" + name + '\'' + ", money=" + money + ", man=" + man + '}';
    }
}
