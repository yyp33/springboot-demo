package com.yyp.springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yyp.springboot.entity.Person;

@RestController
@RequestMapping("/hello")
public class HelloController {

    @Autowired
    Person person1;

    /**
     * 配置文件与对象属性绑定只有在类的成员变量能获取到
     * 方法的局部变量无法获取
     * @param person
     * @return
     */
    @RequestMapping("/springBoot")
    public String springBoot(@Autowired Person person){
        System.out.println(person);
        System.out.println(person1);
        return "Hello Spring Boot";
    }
}
