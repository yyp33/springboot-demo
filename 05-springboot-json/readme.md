# SpringBoot json简单使用

- 常用注解
    - @JsonInclude(JsonInclude.Include.NON_NULL)//如果属性的值为null时，不序列化此属性
    - @JsonProperty("name")//序列化时设置别名
    - @JsonIgnore//序列化时忽略
    - @JsonFormat(pattern = "yyyyMMdd")
- 使用@JsonComponent 可以自定义序列化以及反序列化规则
