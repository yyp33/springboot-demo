package com.yyp.springboot.config;

import java.io.IOException;

import org.springframework.boot.jackson.JsonComponent;
import org.springframework.boot.jackson.JsonObjectDeserializer;
import org.springframework.boot.jackson.JsonObjectSerializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.yyp.springboot.entity.User;

/**
 * 自定义类的序列化以及反序列化
 */
@JsonComponent
public class MyJsonCustom {

    /**
     * 自定义序列化规则
     */
    public static class serializer extends JsonObjectSerializer<User>{
        @Override
        protected void serializeObject(User value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
            jgen.writeObjectField("userId",value.getId());
            jgen.writeObjectField("userName",value.getUserName());
            jgen.writeObjectField("userAge",value.getAge());
        }
    }

    /**
     * 自定义反序列化规则
     */
    public static class Deserializer extends JsonObjectDeserializer<User>{
        @Override
        protected User deserializeObject(JsonParser jsonParser, DeserializationContext context, ObjectCodec codec, JsonNode tree) throws IOException {
            User user = new User();
            user.setId(tree.findValue("userId").asInt());
            return null;
        }
    }
}
