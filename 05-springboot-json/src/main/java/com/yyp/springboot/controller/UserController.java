package com.yyp.springboot.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yyp.springboot.entity.User;

@RestController
@RequestMapping("/user")
public class UserController {

    @PostMapping("/add")
    public User addUser(@RequestBody User user){
        System.out.println(user);
        return user;
    }
}
