# SpringBoot 日志使用

- springBoot 默认使用slf4j座位日志门面，使用logback作为日志实现
- springBoot 对于spring默认使用的jcl+jul日志框架提供jul转slf4j的适配器<br>
    并且提供了log4j转到slf4j的适配器，具体的可以通过看pom文件中类类结构图可以看出
- logback的配置文件有两种明明方式:
    - logback.xml
    - logback-spring.xml 可以获取spring全局配置文件中的配置，设置不同环境等
- logback如果加入自定义的配置文件那么在全局配置文件中的配置将失效
- 
    