package com.yyp.springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yyp.springboot.entity.User;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    MessageSource messageSource;

    @GetMapping("/{id}")
    public User queryUser(@PathVariable Integer id){
        //LocaleContextHolder 就是一个Locale持有器  springmvc底层会自动将LocalResovler中语言设置进去
        String message = messageSource.getMessage("user.query", null, LocaleContextHolder.getLocale());
        User user = new User(1,"zhangsan",message);
        return  user;
    }
}
