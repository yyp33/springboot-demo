package com.yyp.springboot.entity;

public class User {

    private Integer id;
    private String userName;
    private String desc;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", userName='" + userName + '\'' + ", desc='" + desc + '\'' + '}';
    }

    public User() {
    }

    public User(Integer id, String userName, String desc) {
        this.id = id;
        this.userName = userName;
        this.desc = desc;
    }
}
