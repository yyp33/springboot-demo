# 国际化

- springBoot中配置了国际化的自动配置类**MessageSourceAutoConfiguration** 
    - 通过查看源码可以知道如果是自动配置生效需要满足以下条件中的一个
        - 方式一：资源文件中包含以**messages**开头的资源文件
        - 方式二：在配置文件中配置**spring.messages.basename**
- 在WEBMVC的自动配置文件中已经配置好了默认的LocaleResolver，默认实现AcceptHeaderLocaleResolver
   根据请求头中的设置
- 获取文件中信息，自动注入即可
    ```text
     @Autowired
     MessageSource messageSource;
     //LocaleContextHolder 就是一个Locale持有器  springmvc底层会自动将LocalResovler中语言设置进去
     String message = messageSource.getMessage("user.query", null, LocaleContextHolder.getLocale());
    ```