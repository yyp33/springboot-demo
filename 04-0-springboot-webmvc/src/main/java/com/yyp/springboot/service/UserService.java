package com.yyp.springboot.service;

import java.util.List;

import com.yyp.springboot.entity.User;

public interface UserService {

    public void addUser(User user);
    public User getUser(int id);
    public void deleteUser(int id);
    public void modifyUser(User user);
    public List<User> getAllUser();
}
