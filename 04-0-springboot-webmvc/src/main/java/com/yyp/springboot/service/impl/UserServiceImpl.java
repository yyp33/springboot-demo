package com.yyp.springboot.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.yyp.springboot.entity.User;
import com.yyp.springboot.service.UserService;

@Service
public class UserServiceImpl implements UserService {
    public static Map<Integer, User> userMap = new HashMap<>();

    static{
        userMap.put(1,new User(1,"zhangsan",18,"初始化"));
        userMap.put(2,new User(2,"lisi",19,"初始化"));
        userMap.put(3,new User(3,"wangwu",20,"初始化"));
        userMap.put(4,new User(4,"zhaoliu",21,"初始化"));
    }

    @Override
    public void addUser(User user) {
        int userId = userMap.size()+1;
        user.setId(userId);
        userMap.put(userId,user);
    }

    @Override
    public User getUser(int id) {
        return userMap.get(id);
    }

    @Override
    public void deleteUser(int id) {
        userMap.keySet().removeIf(key->key==id);
    }

    @Override
    public void modifyUser(User user) {
       userMap.replace(user.getId(),user);
    }

    @Override
    public List<User> getAllUser() {
        return new ArrayList<>(userMap.values());
    }
}
