package com.yyp.springboot.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class User {

    @JsonInclude(JsonInclude.Include.NON_NULL)//如果属性的值为null时，不序列化此属性
    private Integer id;
    @JsonProperty("name")//序列化时设置别名
    private String userName;
    private Integer age;
    @JsonIgnore//序列化时忽略
    private String desc;
    //序列化时，日期格式化
    @JsonFormat(pattern = "yyyyMMdd")
    private Date birthday;


    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", userName='" + userName + '\'' + ", age=" + age + ", desc='" + desc + '\'' + '}';
    }

    public User() {
    }

    public User(int id, String userName, int age, String desc) {
        this.id = id;
        this.userName = userName;
        this.age = age;
        this.desc = desc;
    }
}
