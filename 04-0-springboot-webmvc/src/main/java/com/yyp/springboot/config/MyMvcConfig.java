package com.yyp.springboot.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.yyp.springboot.interceptor.MyInterceptor;

/**
 * 在自动配置的基础上
 * 增加springmvc扩展功能
 * 比如：增加viewController，增加拦截器
 */
@Configuration
public class MyMvcConfig implements WebMvcConfigurer {

    /**
     * 增加视图控制器
     * @param registry
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/test").setViewName("test");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new MyInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/pages/**");//拦截的路径
    }

    /**
     * 全局CORS配置
     * @param registry
        跨域请求
        也可以使用注解@CrossOrigin("http://localhost:8081")
     @Override
     public void addCorsMappings(CorsRegistry registry) {
     registry.addMapping("/user/*")   // 映射服务器中那些http接口运行跨域访问
     .allowedOrigins("http://localhost:8081")     // 配置哪些来源有权限跨域
     .allowedMethods("GET","POST","DELETE","PUT");   // 配置运行跨域访问的请求方法
     }*/


}
