package com.yyp.springboot.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class HelloController {

    @RequestMapping("/springBoot")
    public String springBoot(){
        return "Hello Spring Boot";
    }
}
