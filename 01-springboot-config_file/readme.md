##Spring Boot 配置文件加载顺序
- 外部配置文件加载顺序 由低-->高<br>
    - classpath文件夹下即resources直接文件加下<br>
    - classPath 的config/文件夹下
    - 项目根目录下（如果项目是父子模块的，项目根目录指父模块下）
    - 项目项目根目录下的config文件夹
    - 运行时置顶目录
    ```java 
    java -jar configuration_file-0.0.1-SNAPSHOT.jar --spring.config.location=D:\config/
    ```
    优先级由低到高，优先级高的配置会覆盖优先级低的配置行程互补<br>
    在相同的文件夹下，不同后缀的配置文件夹加载顺序:**yml>yaml>properties**
- profile文件加载即不同环境配置
    - springboot默认的不同环境的配置文件的名称命名方式<br>
    