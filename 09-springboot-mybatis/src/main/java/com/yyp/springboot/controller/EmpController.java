package com.yyp.springboot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yyp.springboot.mapper.EmpMapper;
import com.yyp.springboot.pojo.Emp;

@Controller
@RequestMapping("/emp")
public class EmpController {

    @Autowired
    private EmpMapper empMapper;


    @GetMapping("/all")
    @ResponseBody
    public List<Emp> getAll(){
        List<Emp> emps = empMapper.selectAll();
        return emps;
    }
}
