package com.yyp.springboot.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yyp.springboot.entity.User;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/user")
@Api("用户管理相关模块")
public class UserController {

    @GetMapping("/{id}")
    @ApiOperation("根据用户ID查询查询用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户id", defaultValue = "99", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 200,message = "查询成功",response = User.class)
    })
    public User getUser(@PathVariable Integer id){
        User user = new User();
        user.setId(1);
        user.setUserName("zhangsan");
        user.setDesc("查询成功");
        return user;
    }
}
