package com.yyp.springboot;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 * MockMvc
 * 测试类中使用，可以在不启动项目的情况下，进行模拟访问测试，方便快捷
 */
@SpringBootTest
@AutoConfigureMockMvc    //专门用于做mockmvc的， 由spring-test提供， 依赖junit5, 如果没有该注解需要通过代码构建MockMvc
public class MockMvcTests {

    @Autowired
    MockMvc mockMvc;

    /**
     *  mockMvc.perform 执行一个请求
     *  ockMvcRequestBuilders.get 发送一个请求；参数：url，url上的参数
     *   .accept(MediaType.APPLICATION_JSON_UTF8) 设置响应的文本为json
     *  .andExpect 增加断言，即对后台返回值的要求
     * .andDo 对返回值所做的操作
     * @throws Exception
     */
    @Test
    void testMockMVCGet() throws Exception {

        // 发起一个模拟请求 ，不依赖网络，不依赖web服务，  不需要启动web应用
        mockMvc.perform(
                MockMvcRequestBuilders.get("/user/{id}", 1)  // 发送了get请求
                        .accept(MediaType.APPLICATION_JSON_UTF8) // 设置响应的文本类型
                //.param(name,value)   ?name=xx&age=xx
        )
                // 响应断言
                .andExpect(MockMvcResultMatchers.status().isOk())   // 断言状态码为200
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.username").value("zhangsanxx"))
                .andDo(MockMvcResultHandlers.print());
    }

    /**
     * MockMvcRequestBuilders.post 发送一个post请求
     *
     * @throws Exception
     */
    @Test
    void testMockMVCPost() throws Exception {

        String userJson = "{\n" +
                "  \"userName\": \"zhaoyi\",\n" +
                "  \"age\": \"18\",\n" +
                "  \"desc\": \"mockmvc\"\n" +
                "}";
        System.out.println(userJson);
        // 发起一个模拟请求 ，不依赖网络，不依赖web服务，  不需要启动web应用
        mockMvc.perform(
                MockMvcRequestBuilders.post("/user/add")  // 发送了get请求
                        .accept(MediaType.APPLICATION_JSON_UTF8) // 设置响应的文本类型
                        .contentType(MediaType.APPLICATION_JSON_UTF8) // 设置请求的文本类型
                        .content(userJson)          // json数据
                //.param(name,value)   ?name=xx&age=xx
        )
                // 响应断言
                .andExpect(MockMvcResultMatchers.status().isOk())   // 断言状态码为200
                //.andExpect(MockMvcResultMatchers.jsonPath("$.data.length()").value(5))
                .andDo(MockMvcResultHandlers.print());

    }
}
