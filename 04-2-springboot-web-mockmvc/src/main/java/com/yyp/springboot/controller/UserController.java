package com.yyp.springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yyp.springboot.Result;
import com.yyp.springboot.entity.User;
import com.yyp.springboot.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/add")
    public Result addUser(@RequestBody User user){
        userService.addUser(user);
        return new Result(200,"增加用户成功",userService.getAllUser());
    }

    @GetMapping("/{id}")
    public Result getUser(@PathVariable("id") Integer id){
        return new Result(200,"查询用户成功",userService.getUser(id));
    }

    @PutMapping("/{id}")
    public Result editUser(@RequestBody User user) {
        userService.modifyUser(user);
        return new Result(200,"修改用户成功",userService.getAllUser());
    }

    @DeleteMapping("/{id}")
    public Result deleteUser(@PathVariable Integer id){
        userService.deleteUser(id);
        return new Result(200,"删除用户成功",userService.getAllUser());
    }
}
