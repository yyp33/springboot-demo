package com.yyp.springboot.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 通过servlet3.0提供的注解配置servlet
 * 这是servlet规范中提供的方式，springboot也提供了注册方式，转至config-》MyMvcConfig
 */
//@WebServlet(name = "myservlet", urlPatterns = "/myServlet")
/*@WebListener 配置监听器
@WebFilter 配置过滤器
*/
public class MyServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("通过注解配置servlet容器");
    }
}
