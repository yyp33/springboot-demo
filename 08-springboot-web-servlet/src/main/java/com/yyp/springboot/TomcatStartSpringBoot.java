package com.yyp.springboot;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * 作用是在Tomcat外部启动时，同时启用springboot相关的配置，
 * 如果不加此类，springboot相关功能在外部tomcat启用war包时，不起作用
 */
public class TomcatStartSpringBoot extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        builder.sources(Application.class);
        return  builder;
    }
}
