package com.yyp.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan//需要在启动类上加入次注解，不然Servlet3.0提供的@webservlet @webFilter @webLister注解无法起作用
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
