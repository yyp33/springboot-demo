package com.yyp.springboot.customs;

import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.stereotype.Component;

/**
 * 对内置容器的属性进行配置
 * 可以使用yml配置文件进行配置
 * server:
    port: 8081

    会与配置文件中进行互补，按照加载顺序，如果相同会覆盖配置文件中的配置
 */
@Component
public class MyCustoms implements WebServerFactoryCustomizer<ConfigurableServletWebServerFactory>{

    @Override
    public void customize(ConfigurableServletWebServerFactory factory) {
        factory.setContextPath("/servlet");
        factory.setPort(8082);
    }
}
