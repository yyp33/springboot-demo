package com.yyp.springboot.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.yyp.springboot.servlet.MyServlet;

/**
 * springboot 提供的注册servlet，注册filter、listening
 */
@Configuration
public class MyMvcConfig {

    /**
     * springboot 提供的方式注册Bean
     * @return
     */
    @Bean
    public ServletRegistrationBean registrationServlet(){
        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean();
        servletRegistrationBean.setServlet(new MyServlet());
        servletRegistrationBean.setName("springboot servlet");
        servletRegistrationBean.addUrlMappings("/springBoot/myServlet");
        return  servletRegistrationBean;
    }

    /**
     * springboot方式注册filter
     *
     * @return

    @Bean
    public FilterRegistrationBean registrationFilter(){
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        return  filterRegistrationBean;
    }*/

    /**
     * springboot方式注册listener
     *
     * @return

    @Bean
    public ServletListenerRegistrationBean registrationListener(){
        ServletListenerRegistrationBean ServletListenerRegistrationBean = new ServletListenerRegistrationBean();
        return  ServletListenerRegistrationBean;
    }*/
}
